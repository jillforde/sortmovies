package jforde.sortmovies;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    MovieSorter sorter = new MovieSorter();
    RecyclerView view;

    public boolean hasPopularMovieBeenSelected = false;
    public boolean hasTopRatedMoviesBeenSelected = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       view  = (RecyclerView) findViewById(R.id.recycle_view);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MovieDisplay movieDisplay = new MovieDisplay();
        view.setAdapter(movieDisplay);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        view.setLayoutManager(layoutManager);

        view.setHasFixedSize(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.all_movies_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_sort_popular){
            hasPopularMovieBeenSelected = true;
        }else {
            hasTopRatedMoviesBeenSelected = true;
        }
        switch (item.getItemId()){
            case R.id.action_sort_popular:
                sorter.sortByPopularity();
                return true;
            case R.id.action_sort_rating:
                sorter.sortByRating();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public boolean getSelectedOption(){
        if(hasTopRatedMoviesBeenSelected){
            return hasPopularMovieBeenSelected;
        }else {
            return hasTopRatedMoviesBeenSelected;
        }
    }


}
