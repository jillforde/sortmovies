package jforde.sortmovies;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jillianforde on 6/1/16.
 */
public class Movie implements Parcelable{
    public String poster_path;
    private String overview;
    private int id;
    private String title;
    private double popularity;
    private int vote_count;
    private double vote_average;

    public Movie(){}

    public Movie(String poster_path, String overview,
                 int id, String title,
                 double popularity, int vote_count,
                 double vote_average) {
        this.poster_path = poster_path;
        this.overview = overview;
        this.id = id;
        this.title = title;
        this.popularity = popularity;
        this.vote_count = vote_count;
        this.vote_average = vote_average;
    }

    private Movie(Parcel in){
        this.poster_path = in.readString();
        this.overview = in.readString();
        this.id = in.readInt();
        this.title = in.readString();
        this.popularity = in.readDouble();
        this.vote_count = in.readInt();
        this.vote_average = in.readDouble();
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    @Override
    public String toString() {
        return poster_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(poster_path);
        dest.writeString(overview);
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeDouble(popularity);
        dest.writeInt(vote_count);
        dest.writeDouble(vote_average);
    }

    public static final Parcelable.Creator<Movie>CREATOR = new Parcelable.Creator<Movie>(){

        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
