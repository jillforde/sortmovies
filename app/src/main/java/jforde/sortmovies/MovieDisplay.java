package jforde.sortmovies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by jillianforde on 6/3/16.
 */

public class MovieDisplay extends RecyclerView.Adapter<MovieDisplay.MovieViewHolder>{
    MovieSorter movieSorter = new MovieSorter();
    MainActivity activity = new MainActivity();
    private final String startOfmoviePosterURL = "http://image.tmdb.org/t/p/w185";
    private String moviePosterURL;
    public ImageView moviePoster;
    Context context;

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_main, parent, false);
        MovieViewHolder viewHolder = new MovieViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        if (activity.getSelectedOption() == activity.hasTopRatedMoviesBeenSelected){
            showTopRatedMovies();
        }else {
            showPopularMovies();
        }
    }

    @Override
    public int getItemCount() {
        return movieSorter.popularMovies.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder{


        public MovieViewHolder(View itemView) {
            super(itemView);
            moviePoster = (ImageView) itemView.findViewById(R.id.img1);
        }
    }
    public void showPopularMovies(){
        for (int i = 0; i < movieSorter.popularMovies.size(); i++) {
            moviePosterURL = startOfmoviePosterURL + movieSorter.popularMovies.get(i).getPoster_path();
            Picasso.with(context).load(moviePosterURL).into(moviePoster);
        }
    }

    public void showTopRatedMovies(){
        for (int i = 0; i < movieSorter.topRatedMovies.size(); i++) {
            movieSorter.topRatedMovies.get(i).getPoster_path();
            moviePosterURL = startOfmoviePosterURL + movieSorter.topRatedMovies.get(i).getPoster_path();
            Picasso.with(context).load(moviePosterURL).into(moviePoster);
        }
    }


}
