package jforde.sortmovies;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by jillianforde on 5/31/16.
 */
public class MovieSorter extends Constants implements SortOptions{
    private static final String TAG = "MovieSorter";
    private final String baseURL = "http://api.themoviedb.org/3/movie/";
    private final String popular = "popular";
    private final String topRated = "top_rated";
    static OkHttpClient client = new OkHttpClient();
    final String popURL = baseURL + popular + apiKey;
    final String topURL = baseURL + topRated + apiKey;
    HashMap<Integer, String> moviePosters = new HashMap<>();
    public ArrayList<Movie> popularMovies = new ArrayList<>();
    public ArrayList<Movie> topRatedMovies = new ArrayList<>();
    boolean populareRequestMade = false;
    boolean ratingRequestMade = false;

    @Override
    public void sortByPopularity() {
        makeRequest(popURL);
        Log.i(TAG, "Sorting by popularity");

    }
    @Override
    public void sortByRating() {
        makeRequest(topURL);
        Log.i(TAG, "SORTING BY RATING");
    }
    private boolean hasRequestBeenMade(String URL){
        switch (URL){
            case topURL:
                if (ratingRequestMade){
                    return true;
                }else {
                    return false;
                }
            case  popURL:
                if(populareRequestMade){
                    return true;
                }else {
                    return false;
                }
        }
        return true;
    }
    private List<Movie> getMovieList(String URL){
        switch (URL) {
            case topURL:
                return topRatedMovies;
            case popURL:
                return popularMovies;
        }
        return null;
    }

    public List<Movie> makeRequest(String URL) {
        if (hasRequestBeenMade(URL)) {
            Log.i(TAG, "Getting movie list");
            getMovieList(URL);
        } else {
            Request request = new Request.Builder()
                    .url(URL)
                    .build();
            Call call = client.newCall(request);
            if(URL == popURL){
                populareRequestMade = true;
            }else {
                ratingRequestMade = true;
            }
            call.enqueue(new Callback() {

                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(TAG, "Shit got real");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.i(TAG, "In on response");
                    try {
                        String jsonData = response.body().string();
                        if (response.isSuccessful()) {

                            JSONObject movieResults = new JSONObject(jsonData);
                            JSONArray movieArr = movieResults.getJSONArray("results");
                            Movie[] movies = new Movie[movieArr.length()];

                            for (int i = 0; i < movieArr.length(); i++) {
                                Movie movie = new Movie();
                                JSONObject m = movieArr.getJSONObject(i);
                                movie.setPoster_path(m.getString("poster_path"));
                                movie.setOverview(m.getString("overview"));
                                movie.setId(m.getInt("id"));
                                movie.setTitle(m.getString("title"));
                                movie.setPopularity(m.getDouble("popularity"));
                                movie.setVote_count(m.getInt("vote_count"));
                                movie.setVote_average(m.getDouble("vote_average"));

                                popularMovies.add(movie);

                                //movies[i] = movie;
                            }

                            //TODO: movies[0] returns the posterpath
                            //TODO: THEY ARE ALL THE SAME MOVIE POSTERS

                            //TODO: movies[1] also returns posterpath
                            ///t2mZzQXjpQxmqtJOPpe8Dr2YpMl.jpg
                            //TODO: IT'S PUTTING THE SAME MOVIE IN OVER AND OVER
                            Log.i(TAG, "Title of movie 1 " + popularMovies.get(0).getTitle());
                            //Log.i(TAG, "Movies from movie array --> " + movies[0]);
                            Log.i(TAG, "TITLE OF movie 2 is " + popularMovies.get(1).getTitle());

                            //Log.i(TAG, "GETTING TITLE " + movies[0].getTitle());
                            //Log.i(TAG, "Getting overview + " + movies[0].getOverview());
                            //Log.i(TAG, "GETTING NEXT TITLE " + movies[1].getTitle());
                        }
                    } catch (JSONException j) {
                        Log.e(TAG, "Error getting movies");
                    }
                }
            });

        }
        return null;
    }
}


