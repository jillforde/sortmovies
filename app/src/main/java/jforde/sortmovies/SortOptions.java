package jforde.sortmovies;

/**
 * Created by jillianforde on 5/31/16.
 */
public interface SortOptions {
    void sortByPopularity();
    void sortByRating();
}
